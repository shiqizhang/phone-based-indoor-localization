function [gt_location]=func_grdtruth(filename_walk, filename_tick, dsamplingrate)
newData1 = importdata(filename_walk);
newData2 = importdata(filename_tick);
% timestamp normalize
newData1.data(:,1) = newData1.data(:,1) - newData1.data(1,1);
newData2.data(:,1) = newData2.data(:,1) - newData2.data(1,1);

[m_sample n_sample] = size(newData1.data);
Data_dsmp = zeros(floor(m_sample/dsamplingrate), n_sample);
j = 1;
for i = 1:m_sample
    if mod(i,dsamplingrate)==0
        Data_dsmp(j,:) = newData1.data(i,:);
        j = j + 1;
    end
end
m_sample = j - 1;
gt_location = zeros(m_sample,3);

for i = 1:m_sample
    gt_location(i,3) = Data_dsmp(i,1);
end

%----------Map Building----------
numOfRows = 720;
numOfCols = 430;
map = zeros(numOfRows, numOfCols);
segs{1} = [344, 328, 96, 328];
segs{2} = [96, 328, 96, 216];
segs{3} = [96 216 96 119];
segs{4} = [96 119 344 119];
segs{5} = [344 119 608 119];
segs{6} = [608 119 608 216];
segs{7} = [608 216 608 328];
segs{8} = [608 328 344 328];
segs{9} = [608 216 344 216];
segs{10} = [344 216 96 216];
segs{11} = [344 119 344 216];
segs{12} = [344 216 344 328];
segs{13} = [608 328 608 390];
segs{14} = [577 328 577 390];
segs{15} = [537 328 537 390];
segs{16} = [502 328 502 390];
segs{17} = [465 328 465 390];
segs{18} = [248 328 248 390];
segs{19} = [208 328 208 390];
segs{20} = [174 328 174 390];
segs{21} = [137 328 137 390];
segs{22} = [96 328 96 390];
segs{23} = [96 287 48 287];
segs{24} = [96 250 48 250];
segs{25} = [96 216 48 216];
segs{26} = [96 173 48 173];
segs{27} = [96 137 48 137];
segs{28} = [96 119 48 119];
segs{29} = [96 119 96 60];
segs{30} = [136 119 136 60];
segs{31} = [176 119 176 60];
segs{32} = [213 119 213 60];
segs{33} = [255 119 255 60];
segs{34} = [458 119 458 60];
segs{35} = [497 119 497 60];
segs{36} = [534 119 534 60];
segs{37} = [573 119 573 60];
segs{38} = [608 119 608 60];
for i = 1 : 38
    minRowIndex = min(segs{i}(1), segs{i}(3));
    maxRowIndex = max(segs{i}(1), segs{i}(3));
    minColIndex = min(segs{i}(2), segs{i}(4));
    maxColIndex = max(segs{i}(2), segs{i}(4));
    map(minRowIndex : maxRowIndex, minColIndex : maxColIndex) = 1;
end

% figure; % plot map
% hold on;
% cnt = 5;
% for i = 1 : size(map, 1)
%     for j = 1 : size(map, 2)
%         if (map(i, j) == 1 && mod(cnt, 5) == 0)
%             plot(j, size(map, 1) + 1 - i,'o');
%         end
%     end
% end
%----------Map Building----------

%----------Tick Points----------
point_tick = zeros(20,2);
point_tick(1,:) = [328,(473+513)/2];
point_tick(2,:) = [328,(547+513)/2];
point_tick(3,:) = [328,(547+584)/2];
point_tick(4,:) = [328,(625+584)/2];
point_tick(5,:) = [(250+287)/2,625];
point_tick(6,:) = [(216+250)/2,625];
point_tick(7,:) = [(173+216)/2,625];
point_tick(8,:) = [(137+173)/2,625];
point_tick(9,:) = [119,(585+625)/2];
point_tick(10,:) = [119,(545+585)/2];
point_tick(11,:) = [119,(508+545)/2];
point_tick(12,:) = [119,(466+508)/2];
point_tick(13,:) = [119,(224+263)/2];
point_tick(14,:) = [119,(187+224)/2];
point_tick(15,:) = [119,(148+187)/2];
point_tick(16,:) = [119,(113+148)/2];
point_tick(17,:) = [328,(113+144)/2];
point_tick(18,:) = [328,(144+184)/2];
point_tick(19,:) = [328,(184+219)/2];
point_tick(20,:) = [328,(219+256)/2];
point_tick(21,:) = [328,(473+513)/2];
%----------Tick Points----------

num_tick = 1;
i=1;
temp_velocity = (point_tick(2,1) - point_tick(1,1) + point_tick(2,2) - point_tick(1,2)) / (newData2.data(2,1) - newData2.data(1,1)); % Initialize the velocity
while (i<=m_sample) && (num_tick<=20)
    if Data_dsmp(i,1)>=newData2.data(1,1) % ignore the beginning data
        if Data_dsmp(i,1)<newData2.data(num_tick+1,1)
            switch num_tick
                case {1, 2, 3, 4, 17, 18, 19, 20}
                    gt_location(i,1) = 328;
                    gt_location(i,2) = temp_velocity * (Data_dsmp(i,1) - newData2.data(num_tick,1)) + point_tick(num_tick,2);
                    if gt_location(i,2)>625
                        gt_location(i,1) = 328 - (gt_location(i,2) - 625);
                        gt_location(i,2) = 625;
                    end
                case {5, 6, 7, 8}
                    gt_location(i,1) = point_tick(num_tick,1) - temp_velocity * (Data_dsmp(i,1) - newData2.data(num_tick,1));
                    gt_location(i,2) = 625;
                    if gt_location(i,1)<119
                        gt_location(i,2) = 625 - (119 - gt_location(i,1));
                        gt_location(i,1) = 119;
                    end
                case {9, 10, 11, 12, 13, 14, 15, 16}
                    gt_location(i,1) = 119;
                    gt_location(i,2) = point_tick(num_tick,2) - temp_velocity * (Data_dsmp(i,1) - newData2.data(num_tick,1));
                    if gt_location(i,2)<113
                        gt_location(i,1) = 119 + (113 - gt_location(i,2));
                        gt_location(i,2) = 113;
                    end
                    if gt_location(i,1)>328
                        gt_location(i,2) = 113 + (gt_location(i,1) - 328);
                        gt_location(i,1) = 328;
                    end
            end
        else
            num_tick = num_tick + 1;
            i = i - 1;
            if num_tick<=20
                if num_tick~=16
                    temp_velocity = (abs((point_tick(num_tick,1) - point_tick(num_tick+1,1))) + abs((point_tick(num_tick,2) - point_tick(num_tick+1,2)))) / (newData2.data(num_tick+1,1)-newData2.data(num_tick,1));
                else
                    temp_velocity = (point_tick(num_tick,2) - 113 + point_tick(num_tick+1,2) - 113 + point_tick(num_tick+1,1) - point_tick(num_tick,1)) / (newData2.data(num_tick+1,1)-newData2.data(num_tick,1));
                end
            end
        end
    end
    i = i + 1;
end

gt_location(:, 2) = 720 - gt_location(:, 2);

end