
%% number of particles is adaptive
function magnetic_2d_adaptive_v2()

% down-sampling
desamplingRate = 5;

drawMapFlag = false;

% the total number of particles
numOfPtcs = 750;

% the percentage of particles randomly generated
newPtcsRatio = 0.03;

% weight decreasing factor, each time a particle runs into walls
wallKillFactor = 0.85;

% whether you want to save the images or not, a binary flag
saveImgFlag = false;

% a flag to identify if the heading of phone is used
dirFlag = true;

% a smaller one corresponds to a larger number of particles, for adaptive PF
delta = 0.25;

speedFactor = 1; % 0.6 is good for Android phones and 1 for Windows phones

winPhoneFlag = true;
androidPhoneFlag = false;

nameOfTest = '2.csv';
nameOfMark = '2.txt';

if (winPhoneFlag)
    [tMagPcs ts] = getTestMagPiece(desamplingRate, nameOfTest);
end

if (androidPhoneFlag)
    tMagPcs = getAndroidMag();
end

h = figure;
isParticlesDrawn = true;
pauseTimeLen = 0.002;
stepLenVar = 1.8;
resamplingFrequency = 1/5;
drawFigureFrequency = 1/5;

%tmpPlot();

[map segs] = initMap(drawMapFlag);
[magMap stepLen] = initMagMap(map, segs);

%exportMagMapForDR(magMap);

ptcs = initPtcs(numOfPtcs, map);
stepLen = stepLen * desamplingRate;
stepLen = stepLen * speedFactor;
testLength = size(tMagPcs, 1);

posList = zeros(testLength, 2);
var2D = zeros(testLength, 1);
for i = 1 : testLength
    [ptcs quality] = updateWeights(ptcs, tMagPcs(i, :), map, magMap, wallKillFactor);
    
    if (mod(i, 1/resamplingFrequency) == 0)
        %ptcs = addMorePtcs(ptcs, newPtcsRatio, map);
        numOfPtcs = getNumOfPtcs(map, ptcs, delta);
        fprintf('Number of particles: %d\n', numOfPtcs);
        ptcs = resampling(ptcs, numOfPtcs);
    end
    
    if (mod(i, 1/drawFigureFrequency) == 0)
        drawFigure(h, segs, map, ptcs, isParticlesDrawn, magMap, tMagPcs(i, :));
        if (saveImgFlag)
            saveas(h, strcat('images/img', sprintf('%05d', i)), 'jpg');
        end
        pause(pauseTimeLen);
        drawnow;
    end
    
    posList(i, 1:2) = getPosOfBestPtc(ptcs);
    var2D(i) = getVar2D(ptcs);
    
    ptcs = ptcsMove(ptcs, map, tMagPcs(i, :), stepLenVar, stepLen, dirFlag);
end
[gt_location] = func_grdtruth(nameOfTest, nameOfMark, desamplingRate);

plotError([ts', posList], gt_location, var2D);
end

%%
function var2D = getVar2D(ptcs)
ptcsMat = zeros(size(ptcs, 2), 2);
for i = 1 : size(ptcs, 2)
    ptcsMat(i, 1:2) = ptcs{i}(1:2);
end

var2D = norm(cov(ptcsMat));

end

%% 
function plotError(posList, gt_location, var2D)
bjwErr = getErrorCurveBJW(posList(:, 2:3), gt_location);
timeInSec = (gt_location(size(gt_location, 1), 3) - gt_location(1, 3))/(10^7);
figure;
plot(timeInSec*((1:size(bjwErr, 2))/size(bjwErr, 2)), bjwErr/(512/60), ...
    'LineWidth', 2);
set(gca,'FontSize',20);
xlhand = get(gca,'xlabel');
set(xlhand,'string','Time (in second)','fontsize',25);
ylhand = get(gca,'ylabel');
set(ylhand,'string','Error (in meter)','fontsize',25);

for i = 1 : size(bjwErr, 2)
    if (bjwErr(i)/(512/60) < 15)
        timeNeededForLoc = i;
        timeNeededForLoc = timeInSec * timeNeededForLoc / size(bjwErr, 2);
        sumError = sum(bjwErr(i:size(bjwErr, 2))) / (size(bjwErr, 2) - i);
        grid on;
        break;
    end
end

% plot CDF
figure;
cdfplot(bjwErr/(512/60));
set(gca,'FontSize',20);
xlhand = get(gca,'xlabel');
set(xlhand,'string','Error (in meter)','fontsize',25);
ylhand = get(gca,'ylabel');
set(ylhand,'string','Percentage','fontsize',25);

% plot covariance curve
figure;
plot(timeInSec*((1:size(var2D, 1))/size(var2D, 1)), var2D/(512/60), 'LineWidth', 2);
hold on;
for i = 1 : size(var2D, 1)
    if (var2D(i) < 0.5*10000)
        plot(ones(1, 2) * timeInSec * i / size(var2D, 1), [0 4*10000]/(512/60), 'r', 'LineWidth', 2);
        break;
    end
end
grid on;
set(gca,'FontSize',20);
xlhand = get(gca,'xlabel');
set(xlhand,'string','Time (in second)','fontsize',25);
ylhand = get(gca,'ylabel');
set(ylhand,'string','Norm of covariance matrix of particle positions','fontsize',25);

end

%%
function bjwErr = getErrorCurveBJW(posList, gtLoc)

lenOfGt = size(gtLoc, 1);
lenOfTest = size(posList, 1);

gtLoc = gtLoc(lenOfGt - lenOfTest + 1 : lenOfGt, :);

for i = 1 : size(gtLoc, 1)
    if (abs(gtLoc(i, 1)) == 0 && abs(gtLoc(i, 2)) == 720)
        break;
    end
    
    bjwErr(i) = sqrt((posList(i, 1) - gtLoc(i, 2))^2 + (posList(i, 2) - gtLoc(i, 1))^2);
end

end

%%
function maxPos = getPosOfBestPtc(ptcs)

maxPos = zeros(1, 2);
maxWeight = 0;

for i = 1 : size(ptcs, 2)
    if (ptcs{i}(3) > maxWeight)
        maxWeight = ptcs{i}(3);
        maxPos = ptcs{i}(1:2);
    end
end

end

%%
function numOfPtcs = getNumOfPtcs(map, ptcs, delta)

cnt = 0;
recordMap = 0 * map;
for i = 1 : size(ptcs, 2)
    if (recordMap(ptcs{i}(1), ptcs{i}(2)) == 0)
        recordMap(ptcs{i}(1), ptcs{i}(2)) = 1;
        cnt = cnt + 1;
    end
end

x = -10:0.1:10;
y = gaussmf(x,[1 0]);

yAcc(1) = y(1);
for i = 2 : size(y, 2)
    yAcc(i) = yAcc(i - 1) + y(i);
    if (yAcc(i) > 1 - delta)
        quantile = (i-1)/10;
        break;
    end
end

x = cnt;
% Paper: KLD-Sampling: Adaptive Particle Filters
% by Dieter Fox, NIPS, 2001
y = ((x-1)/(2*delta))*(1-(2/(9*(x-1)))+((2/(9*(x-1)))^0.5)*quantile)^3;
numOfPtcs = round(y);

end
%%
function newPtcs = resampling(ptcs, numOfPtcs)

weights = zeros(1, size(ptcs, 2));

for i = 1 : size(ptcs, 2)
    weights(1, i) = ptcs{i}(3);
end

indexOfPtcs = discretesample(weights, numOfPtcs);

newPtcs = cell(1, numOfPtcs);
for i = 1 : numOfPtcs
    newPtcs{i} = ptcs{indexOfPtcs(i)};
end
end

%%
function ptcs = addMorePtcs(ptcs, ratio, map)

numOfPtcs = size(ptcs, 2);
numOfNewPtcs = fix(numOfPtcs * ratio);

uniDist = map/sum(sum(map));
newPtcsPostions = drawSampleFrom2dDist(uniDist, numOfNewPtcs);

for i = numOfPtcs + 1 : numOfPtcs + numOfNewPtcs
    ptcs{i}(1) = newPtcsPostions(i - numOfPtcs, 1);
    ptcs{i}(2) = newPtcsPostions(i - numOfPtcs, 2);
    ptcs{i}(3) = 1 / (numOfPtcs + numOfNewPtcs);
end

sumWeight = 0;

for i = 1 : numOfPtcs + numOfNewPtcs
    sumWeight = sumWeight + ptcs{i}(3);
end

for i = 1 : numOfPtcs + numOfNewPtcs
    ptcs{i}(3) = ptcs{i}(3) / sumWeight;
end

end

%% 
function drawFigure(h, segs, map, ptcs, isParticlesDrawn, magMap, tMagPc)

tMagPc(4) = sqrt(tMagPc(1)^2 + tMagPc(2)^2);
tMagPc(5) = sqrt(tMagPc(1)^2 + tMagPc(2)^2 + tMagPc(3)^2);

hold off;
numOfRows = size(map, 1);
numOfCols = size(map, 2);

% set the map
plot3(0, 0, 0);
hold on;
set(gca, 'XTick', [], 'YTick', [], 'ZTick', []);

mgn = 60;
axis([-mgn, size(map, 2) + mgn, mgn, size(map, 1) + mgn, -40 40]);

% draw the background image
% Move the background axes to the bottom
%ha = axes('units','normalized', 'position',[-mgn, size(map, 2) + mgn, mgn, size(map, 1) + mgn]);
%uistack(h,'bottom');

% Load in a background image and display it using the correct colors
% The image used below, is in the Image Processing Toolbox. If you do not have %access to this toolbox, you can use another image file instead.
%I=imread('BJW12F.png');
%hi = imagesc(I);
%colormap gray;

% Turn the handlevisibility off so that we don't inadvertently plot into the axes again
% Also, make the axes invisible
%set(ha,'handlevisibility','off', 'visible','off')

% Now we can use the figure, as required.
% For example, we can put a plot in an axes
%axes('position',[-mgn, size(map, 2) + mgn, mgn, size(map, 1) + mgn])

% draw map with lines
numOfSegs = size(segs, 2);
for i = 1 : numOfSegs
    plot3([segs{i}(2), segs{i}(4)], ...
        [numOfRows + 1 - segs{i}(1), numOfRows + 1 - segs{i}(3)], ...
        [0 0], 'LineWidth', 1);
end

zFlag = false;
sqrtXyFlag = false;
sqrtXyzFlag = false;

% find the particle with maximum weight
maxIndex = 0;
maxWeight = 0;
numOfPtcs = size(ptcs, 2);
for i = 1 : numOfPtcs
    if (maxWeight < ptcs{i}(3))
        maxWeight = ptcs{i}(3);
        maxIndex = i;
    end
end

% plot the position with maximum weight
plot3(ptcs{maxIndex}(2), numOfRows - ptcs{maxIndex}(1) + 1, 0, ...
    'ro', 'MarkerSize', 8, 'LineWidth', 2);

if (zFlag)
    plot3(ones(1, 2) * ptcs{maxIndex}(2), ...
        ones(1, 2) * (numOfRows - ptcs{maxIndex}(1) + 1), ...
        [0 tMagPc(3)], 'k', 'MarkerSize',5, 'LineWidth', 2);
    text(1, 1, 100,'Z');
end

if (sqrtXyFlag)
    plot3(ones(1, 2) * ptcs{maxIndex}(2), ...
        ones(1, 2) * (numOfRows - ptcs{maxIndex}(1) + 1), ...
        [0 tMagPc(4)], 'k', 'MarkerSize',5, 'LineWidth', 3);
    text(1, 1, 100,'Sqrt(X^2 + Y^2)');
end

if (sqrtXyzFlag)
    plot3(ones(1, 2) * ptcs{maxIndex}(2), ...
        ones(1, 2) * (numOfRows - ptcs{maxIndex}(1) + 1), ...
        [0 tMagPc(5)], 'k', 'MarkerSize',5, 'LineWidth', 3);
    text(1, 1, 100,'sqrt(X^2 + Y^2 + Z^2)');
end

% draw walking direction
xyValue = [tMagPc(1) tMagPc(2)] * [0 -1/17; -1/17 0];
inflationFactor = 70;
xV = xyValue(1)*inflationFactor;
yV = xyValue(2)*inflationFactor;
originX = 0;
originY = 200;
plot3(originX, originY, 0, 'ko', 'MarkerSize',10, 'LineWidth', 2);

plot3([originX, originX + xV], [originY, originY + yV], [0 0], 'LineWidth', 2);
dir = getWalkingDir(tMagPc(1:3));
switch dir
    case 1
        plot3([originX, originX], [originY, originY + inflationFactor], ...
            [0 0], 'r', 'LineWidth', 2);
    case 2
        plot3([originX, originX], [originY, originY - inflationFactor], ...
            [0 0], 'r', 'LineWidth', 2);        
    case 3
        plot3([originX, originX - inflationFactor], [originY, originY], ...
            [0 0], 'r', 'LineWidth', 2);        
    case 4
        plot3([originX, originX + inflationFactor], [originY, originY], ...
            [0 0], 'r', 'LineWidth', 2);                
end

% preparation for the small line segments
ptcsCnt = map * 0;
for i = 1 : size(ptcs, 2)
    ptcsCnt(ptcs{i}(1), ptcs{i}(2)) = 1 + ptcsCnt(ptcs{i}(1), ptcs{i}(2));
end

if (~isParticlesDrawn)    
    return;    
end

% Particles are placed from top-left corner, the following is just for
% drawing this figure. Draw the small line segments based on particle
% number. 

cnt = 1;
for i = 1 : size(map, 1)
    for j = 1 : size(map, 2)
        if (map(i, j) == 0 || ptcsCnt(i, j) == 0)
            continue;
        end
        
        if (mod(cnt, 3) == 0)
            plot3(ones(1, 2) * j, ones(1, 2) * ...
                (numOfRows + 1 - i), [0, ptcsCnt(i, j)], 'c');
        end
        cnt = cnt + 1;
    end
end

end

%% 
function ptcs = ptcsMove(ptcs, map, mag, var, stepLen, dirFlag)
numOfPtcs = size(ptcs, 2);

if (dirFlag)
    dir = getWalkingDir(mag);
else
    dir = randperm(4);
    dir = dir(1);
end

for i = 1 : numOfPtcs
    ptcs{i}(1) = round(ptcs{i}(1));
    ptcs{i}(2) = round(ptcs{i}(2));
    
    assert(map(ptcs{i}(1), ptcs{i}(2)) == 1);
    
    switch dir
        case 0
            stpLenDnwd = round(normrnd(0, var));
            stpLenRtwd = round(normrnd(0, var));
        case 1
            stpLenDnwd = round(normrnd(-stepLen, var));
            stpLenRtwd = round(normrnd(0, var));
        case 2
            stpLenDnwd = round(normrnd(stepLen, var));
            stpLenRtwd = round(normrnd(0, var));
        case 3
            stpLenDnwd = round(normrnd(0, var));
            stpLenRtwd = round(normrnd(-stepLen, var));            
        case 4
            stpLenDnwd = round(normrnd(0, var));
            stpLenRtwd = round(normrnd(stepLen, var));
        otherwise
            fprintf('ptcsMove error\n');
            return;
    end
    
    ptcs{i}(1:2) = projPtToMap(map, ...
        [ptcs{i}(1) + stpLenDnwd, ptcs{i}(2) + stpLenRtwd]);
    
    assert(map(ptcs{i}(1), ptcs{i}(2)) == 1);
end

end

%%
function ptOnMap = projPtToMap(map, pt)
numOfRows = size(map, 1);
numOfCols = size(map, 2);

pt(1) = max(pt(1), 1);
pt(1) = min(pt(1), numOfRows);
pt(2) = max(pt(2), 1);
pt(2) = min(pt(2), numOfCols);

if(map(pt(1), pt(2)) == 1)
    ptOnMap = pt;
    return;
end

dis = 1;
while(1)
    order = randperm(8);
%    ptAbove = [pt(1) - dis, pt(2)];
%    ptBelow = [pt(1) + dis, pt(2)];
%    ptLeftSide = [pt(1), pt(2) - dis];
%    ptRightSide = [pt(1), pt(2) + dis];
%    ptTopLeft = [pt(1) - dis, pt(2) - dis];
%    ptTopRight = [pt(1) - dis, pt(2) + dis];
%    ptBotLeft = [pt(1) + dis, pt(2) - dis];
%    ptBotRight = [pt(1) + dis, pt(2) + dis];
    ptNearby = cell(1, size(order, 2));
    ptNearby{1} = [pt(1) - dis, pt(2)];
    ptNearby{2} = [pt(1) + dis, pt(2)];
    ptNearby{3} = [pt(1), pt(2) - dis];
    ptNearby{4} = [pt(1), pt(2) + dis];
    ptNearby{5} = [pt(1) - dis, pt(2) - dis];
    ptNearby{6} = [pt(1) - dis, pt(2) + dis];
    ptNearby{7} = [pt(1) + dis, pt(2) - dis];
    ptNearby{8} = [pt(1) + dis, pt(2) + dis];

    for i = 1 : size(order, 2)
        ptNearby{i}(1) = max(ptNearby{i}(1), 1);
        ptNearby{i}(1) = min(ptNearby{i}(1), numOfRows);
        ptNearby{i}(2) = max(ptNearby{i}(2), 1);
        ptNearby{i}(2) = min(ptNearby{i}(2), numOfCols);
        if (map(ptNearby{i}(1), ptNearby{i}(2)) == 1)
            ptOnMap = ptNearby{i};
            return;
        end
    end
    dis = dis + 1;
end

end

%%
function [dir dis] = getWalkingDir(mag)

dis = zeros(1, 4);
dis(1) = pdist([mag(1:2); -20 0]); % upward
dis(2) = pdist([mag(1:2); 20 0]); % downward
dis(3) = pdist([mag(1:2); 0 20]); % leftward
dis(4) = pdist([mag(1:2); 0 -20]); % rgithward

[minValue, dir] = min(dis);
if (minValue > 20)
    dir = 0;
end

end
%%
function [ptcs quality] = updateWeights(ptcs, mag, map, magMap, wallKillFactor)

totalWeight = 0;
quality = 0;

dir = getWalkingDir(mag);

pNum = size(ptcs, 2);
transMatrix = [0 -1/17; -1/17 0];
for i = 1 : pNum
    
    switch dir
        case 1
            if (map(ptcs{i}(1) - 1, ptcs{i}(2)) == 0)
                ptcs{i}(3) = ptcs{i}(3) * wallKillFactor;
            end
        case 2
            if (map(ptcs{i}(1) + 1, ptcs{i}(2)) == 0)
                ptcs{i}(3) = ptcs{i}(3) * wallKillFactor;
            end
        case 3
            if (map(ptcs{i}(1), ptcs{i}(2) - 1) == 0)
                ptcs{i}(3) = ptcs{i}(3) * wallKillFactor;
            end
        case 4
            if (map(ptcs{i}(1), ptcs{i}(2) + 1) == 0)
                ptcs{i}(3) = ptcs{i}(3) * wallKillFactor;
            end
    end
    
    magBase = magMap(ptcs{i}(1), ptcs{i}(2), :);
    %gauValueX{i} = getGauValue(mag(1, 1), magBase(1));
    %gauValueY{i} = getGauValue(mag(1, 2), magBase(2));
    %gauValueZ{i} = getGauValue(mag(1, 3), magBase(3));
    %gauValueXY{i} = getGauValue(sqrt(mag(1, 1)^2 + mag(1, 2)^2), sqrt(magBase(1, 1, 1)^2 + magBase(1, 1, 2)^2));

    %oriBase = [magBase(1, 1, 1), magBase(1, 1, 2)] * transMatrix;
    %oriBase = oriBase / sqrt(oriBase(1)^2 + oriBase(2)^2);
    
    % -2/pi ~ 2/pi
    %baseAngle = atan(oriBase(2) / oriBase(1)); 
    
    %oriTest = mag(1:2) * transMatrix;
    %oriTest = oriTest / sqrt(oriTest(1)^2 + oriTest(2)^2);
    
    % -2/pi ~ 2/pi
    %testAngle = atan(oriTest(2) / oriTest(1));
    
    %baseAngle = 10 * abs(baseAngle);
    %testAngle = 10 * abs(testAngle);
    
    %gauValueOri{i} = getGauValue(baseAngle, testAngle);
    gauValueXYZ{i} = getGauValue(sqrt(mag(1, 1)^2 + mag(1, 2)^2 + mag(1, 3)^2), ...
        sqrt(magBase(1, 1, 1)^2 + (magBase(1, 1, 2)^2) + (magBase(1, 1, 3))^2));
    
    %ptcs{i}(3) = ptcs{i}(3) * gauValueXY{i} * gauValueZ{i};
    ptcs{i}(3) = ptcs{i}(3) * gauValueXYZ{i};
    %ptcs{i}(3) = ptcs{i}(3) * gauValueXYZ{i} * gauValueOri{i};
    %ptcs{i}(3) = ptcs{i}(3) * gauValueXY{i} * gauValueZ{i} * gauValueXYZ{i} * gauValueOri{i};
    quality = quality + gauValueXYZ{i};
    totalWeight = totalWeight + ptcs{i}(3);
end

for i = 1 : pNum
    ptcs{i}(3) = ptcs{i}(3) / totalWeight;
end

end
%%
function [magMap stepLen] = initMagMap(map, segs)

path = 'BJW/';

numOfMagFiles = 29;

for i = 1 : numOfMagFiles
    csvName = strcat(path, '1-', num2str(i), '.csv');
    txtName = strcat(path, '1-', num2str(i), '.txt');
    if (i == 1)
        magPieces = getMagPieces(1, csvName, txtName);
        fprintf('File %d has provided %d pieces.\n', i, size(magPieces, 2));
    else
        newMagPieces = getMagPieces(1, csvName, txtName);
        magPieces = {magPieces{:} newMagPieces{:}};
        fprintf('File %d has provided %d pieces.\n', i, size(newMagPieces, 2));
    end
end

numOfMagPieces = size(magPieces, 2);
% [startRowIndex startColIndex endRowIndex endColIndex]
sttEnds = zeros(numOfMagPieces, 4);
for i = 1 : numOfMagPieces
    sttEnds(i, :) = segs{i};
end

% magMap(i, j, 4) is the sqrt(x^2 + y^2)
% magMap(i, j, 5) is the sqrt(x^2 + y^2 + z^2)
magMap = zeros(size(map, 1), size(map, 2), 5);
for i = 1 : numOfMagPieces
    length = pdist([sttEnds(i, 1:2); sttEnds(i, 3:4)]);
    direction = getMagPieceDirection(sttEnds(i, :));
    
    newMagPiece = reDistrMag(magPieces{i}, length + 1, direction, i);
    
    minRow = min(sttEnds(i, 1), sttEnds(i, 3));
    maxRow = max(sttEnds(i, 1), sttEnds(i, 3));
    minCol = min(sttEnds(i, 2), sttEnds(i, 4));
    maxCol = max(sttEnds(i, 2), sttEnds(i, 4));    
    magMap(minRow : maxRow, minCol : maxCol, :) = newMagPiece;
end

stepLen = 250 / size(magPieces{1}, 1);

end
%%
% to re-organize the magnetic data in order to be added to magMap directly
function newMagPiece = reDistrMag(magPiece, outLen, dir, pieceIndex)

inLen = size(magPiece, 1);

for i = 1 : inLen
    magPiece(i, 4) = sqrt(magPiece(i, 1)^2 + magPiece(i, 2)^2);
    magPiece(i, 5) = sqrt(magPiece(i, 1)^2 + magPiece(i, 2)^2 + magPiece(i, 3)^2);
end

switch dir
    case 1 % upward
        newMagPiece = zeros(outLen, 1, 5);
    case 2 % downward
        newMagPiece = zeros(outLen, 1, 5);
    case 3 % leftward
        newMagPiece = zeros(1, outLen, 5);
    case 4 % rightward
        newMagPiece = zeros(1, outLen, 5);
end

for i = 1 : outLen
    posOnUnit = i / outLen;
    smlstDis = 10000000;
    nrstIndex = 0;
    for j = 1 : inLen
        dis = abs(posOnUnit - (j / inLen));
        if (dis < smlstDis)
            nrstIndex = j;
            smlstDis = dis;
        end
    end
    
    assert(nrstIndex ~= 0);
    switch dir
        case 1 % upward
            newMagPiece(outLen + 1 - i, 1, :) = magPiece(nrstIndex, :);
        case 2 % downward
            newMagPiece(i, 1, :) = magPiece(nrstIndex, :);
        case 3 % leftward
            newMagPiece(1, outLen + 1 - i, :) = magPiece(nrstIndex, :);
        case 4 % rightward
            newMagPiece(1, i, :) = magPiece(nrstIndex, :);
        otherwise
            fprintf('reDistrMag error: type 2\n');
    end
end

drawFigureFlag = false;
if (drawFigureFlag)
    figure(pieceIndex * 100);
    plot(magPiece);
    [len, vertOrHori] = ...
        max([size(newMagPiece, 1), size(newMagPiece, 2)]);
    magToPlot = zeros(len, 4);
    figure(pieceIndex*100 + 1);
    plot(squeeze (newMagPiece));
end

end
%%
% 1: upward, 2: downward, 3: leftward, 4: rightward
function direction = getMagPieceDirection(sttEnd)
if (sttEnd(2) == sttEnd(4) && sttEnd(1) > sttEnd(3))
    direction = 1;
elseif (sttEnd(2) == sttEnd(4) && sttEnd(1) < sttEnd(3))
    direction = 2;
elseif (sttEnd(2) > sttEnd(4) && sttEnd(1) == sttEnd(3))
    direction = 3;
elseif (sttEnd(2) < sttEnd(4) && sttEnd(1) == sttEnd(3))
    direction = 4;
else
    fprintf('getMagPieceDirection error\n');
end

end

%%
function ptcs = initPtcs(num, map)

rowColOfPtcs = drawSampleFrom2dDist(map, num);

weight = 1/num;
ptcs = cell(1, num);
for i = 1 : num
    ptcs{i} = zeros(1, 3);
    ptcs{i}(1) = rowColOfPtcs(i, 1);
    ptcs{i}(2) = rowColOfPtcs(i, 2);
    ptcs{i}(3) = weight;
end
end

%%
function rowColOfPtcs = drawSampleFrom2dDist(dist2d, numOfPtcs)
numOfRows = size(dist2d, 1);
numOfColumns = size(dist2d, 2);

dist = zeros(1, numOfRows * numOfColumns);
for i = 1 : numOfRows
    dist(1, (i - 1) * numOfColumns + 1 : i * numOfColumns) = dist2d(i, :);
end

indexOfPtcs = discretesample(dist, numOfPtcs);

rowColOfPtcs = zeros(numOfPtcs, 2);
for i = 1 : numOfPtcs
    rowColOfPtcs(i, 1) = ceil(indexOfPtcs(i) / numOfColumns);
    rowColOfPtcs(i, 2) = mod(indexOfPtcs(i) - 1, numOfColumns) + 1;
end

end

%%
function [map segs] = initMap(drawMapFlag)

% for convinience to comparing with the JPG image, the (1,1) point is set
% at the top-left conner. (x, y) means the column number is x, and the row
% number is y - top-down manner.  

numOfRows = 720;
numOfCols = 430;

map = zeros(numOfRows, numOfCols);

% [colIndexStart, rowIndexStart, colIndexEnd, rowIndexEnd]
segs{1} = [344, 328, 96, 328];
segs{2} = [96, 328, 96, 216];
segs{3} = [96 216 96 119];
segs{4} = [96 119 344 119];
segs{5} = [344 119 608 119];
segs{6} = [608 119 608 216];
segs{7} = [608 216 608 328];
segs{8} = [608 328 344 328];
segs{9} = [608 216 344 216];
segs{10} = [344 216 96 216];
segs{11} = [344 119 344 216];
segs{12} = [344 216 344 328];
segs{13} = [608 328 608 390];
segs{14} = [577 328 577 390];
segs{15} = [537 328 537 390];
segs{16} = [502 328 502 390];
segs{17} = [465 328 465 390];
segs{18} = [248 328 248 390];
segs{19} = [208 328 208 390];
segs{20} = [174 328 174 390];
segs{21} = [137 328 137 390];
segs{22} = [96 328 96 390];
segs{23} = [96 287 48 287];
segs{24} = [96 250 48 250];
segs{25} = [96 216 48 216];
segs{26} = [96 173 48 173];
segs{27} = [96 137 48 137];
segs{28} = [96 119 48 119];
segs{29} = [96 119 96 60];
segs{30} = [136 119 136 60];
segs{31} = [176 119 176 60];
segs{32} = [213 119 213 60];
segs{33} = [255 119 255 60];
segs{34} = [458 119 458 60];
segs{35} = [497 119 497 60];
segs{36} = [534 119 534 60];
segs{37} = [573 119 573 60];
segs{38} = [608 119 608 60];

for i = 1 : 38
    minRowIndex = min(segs{i}(1), segs{i}(3));
    maxRowIndex = max(segs{i}(1), segs{i}(3));
    minColIndex = min(segs{i}(2), segs{i}(4));
    maxColIndex = max(segs{i}(2), segs{i}(4));    
    map(minRowIndex : maxRowIndex, minColIndex : maxColIndex) = 1;
end

if (~drawMapFlag)
    return;
end

figure;
hold on;

cnt = 5;
for i = 1 : size(map, 1)
    for j = 1 : size(map, 2)
        if (map(i, j) == 1 && mod(cnt, 5) == 0)
            plot(j, size(map, 1) + 1 - i,'o');
        end
    end
end
end

%% 
% longP1 is the external circle counter-clockwise starts/ends at 12257
% longP2 is from the server room to the other end
% longP3 is from the stairs to 12257
function magP = getMagPieces(desamplingRate, longP, longP_mark)

p = importdata(longP);
p = p.data;
timeStamps = p(:, 1);
mags = p(:, 5:7);
marks = importdata(longP_mark);
marks = marks.data;

numOfDataL = size(p, 1);
numOfMarks = size(marks, 1);
markIndex = 1;
indexForMarks = zeros(1, numOfMarks);

for i = 1 : numOfMarks
    for j = 2 : numOfDataL
        if (markIndex > numOfMarks)
            break;
        end

        if (timeStamps(j - 1) <= marks(markIndex) && ...
                marks(markIndex) <= timeStamps(j))
            indexForMarks(markIndex) = j;
            markIndex = markIndex + 1;
        end
    end
end

magPieces = cell(1, numOfMarks);
for i = 1 : numOfMarks - 1
    magPieces{i} = mags(indexForMarks(i) : indexForMarks(i + 1), :);
end

for i = 1 : size(magPieces, 2)
    cnt = 1;
    for j = 1 : size(magPieces{i}, 1)
        if (mod(j, desamplingRate) == 0)
            magP{i}(cnt, :) = magPieces{i}(j, :);
            cnt = cnt + 1;
        end
    end
end

end
%%
function [mag ts] = getTestMagPiece(desamplingRate, filename)

p = importdata(filename);

p = p.data;
mapL = size(p, 1);

cnt = 1;
for j = 1 : mapL
    if(mod(j, desamplingRate) == 0)
        if (abs(p(j, 5)) + abs(p(j, 5)) + abs(p(j, 5)) ~= 0)
            % mag represents the magnetic
            ts(cnt) = p(j, 1);
            mag(cnt, 1) = p(j, 5);
            mag(cnt, 2) = p(j, 6);
            mag(cnt, 3) = p(j, 7);
            mag(cnt, 4) = sqrt(mag(cnt, 1)^2 + mag(cnt, 2)^2 + mag(cnt, 3)^2);
            cnt = cnt + 1;
        else
            continue;
        end
    end
end

%figure;
%plot(tMagPcs1(500:5:size(tMagPcs1, 1), :));

end

%%
function tMagPcs = getAndroidMag()
tMagPcs = importdata('D:\workspace\data\0812_androidWinPhone\twoPhonesOnChairSideBySide\EXP12_IMU.txt');
tMagPcs = tMagPcs(:, 8:10);
tMagPcs = tMagPcs(1:size(tMagPcs, 1), :);
cnt = 1;

newTMagPcs = zeros(1, 3);

for i = 1 : 1 : size(tMagPcs, 1)
    newTMagPcs(cnt, :) = tMagPcs(i, :);
    cnt = cnt + 1;
end
tMagPcs = newTMagPcs;
tMagPcs(:, 4) = tMagPcs(:, 1).^2 + tMagPcs(:, 2).^2 + tMagPcs(:, 3).^2;
figure;
tMagPcs(:,4) = sqrt(tMagPcs(:, 4));
%plot(tMagPcs(500:5:size(tMagPcs, 1), :));
end

%%
function v = getGauValue(v1, v2)
c = 20; % really not sure how to set this value

v = 2.71828^(-(v1-v2)*(v1-v2)/(2*c^2));

end

%%
function tmpPlot()

desamplingRate = 3;

t{1} = getTestMagPiece(desamplingRate,'D:\workspace\data\0826_wideCorridor\short_right_0.csv');
t{2} = getTestMagPiece(desamplingRate,'D:\workspace\data\0808_18_4times\1.csv');
t{3} = getTestMagPiece(desamplingRate,'D:\workspace\data\0808_18_4times\2.csv');
t{4} = getTestMagPiece(desamplingRate,'D:\workspace\data\0808_18_4times\3.csv');

figure;

for j = 1 : 4
    new{j} = reDistrMag(t{j}, 100, 4, 0);
    for i = 1 : size(new{j}, 1)
        new{j}(i, 4) = sqrt(new{j}(i, 1)^2 + new{j}(i, 2)^2 + new{j}(i, 3)^2);
        new{j}(i, 5) = sqrt(new{j}(i, 1)^2 + new{j}(i, 2)^2);
    end
end

plot(0, 0);
hold on;

plot(new{1}(1, :, 4), 'r');
%plot(new{2}(1, :, 4), 'g');
%plot(new{3}(1, :, 4), 'b');
%plot(new{4}(1, :, 4), 'k');

end


